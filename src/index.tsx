import React from "react";
import ReactDOM from "react-dom";
// import Header from './components/Header';
// import Footer from './components/Footer';
import './scss/app.scss';
import { DndProvider } from 'react-dnd';
import Backend from 'react-dnd-html5-backend';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Node from "./components/node";
import Board from "./components/board";
// import Home from "./components/Home/index";
// import About from "./components/About";
// import Users from "./components/Users";
// import { Provider } from "./context/commonContext";
// import List from "./components/List";
// import dotenv from 'dotenv';
// dotenv.config();

const App = (): JSX.Element => {
  return (
    <Router>
      {/* <Provider> */}
      <DndProvider backend={Backend}>
        <Switch>
          <Route path="/">
            <div className="relative">
              <Board />
            </div>
          </Route>
        </Switch>
      </DndProvider>
      {/* </Provider> */}
    </Router>
  )
}

let element = document.getElementById("app");

ReactDOM.render(<App />, element);