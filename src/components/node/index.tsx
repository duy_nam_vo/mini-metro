import React, { useState, useEffect } from 'react';
import { Coordinate } from '../../utils/types';

interface Props {
  X: number;
  Y: number;
  initialSize: number;
  addNode(coordinate: Coordinate): void;
}

const edgeSize = 100;
const animationSpeed = 500;
const transition = { transition: `width ${animationSpeed/1000}s, left ${animationSpeed/1000}s, top ${animationSpeed/1000}s,height ${animationSpeed/1000}s, transform ${animationSpeed/1000}s` }
const Node = ({ X, Y, initialSize, addNode }: Props) => {
  const [size, setSize] = useState(initialSize);
  const [rightEdgeSize, setRightEdgeSize] = useState(0);
  const [leftEdgeSize, setLeftEdgeSize] = useState(0);
  const [topEdgeSize, setTopEdgeSize] = useState(0);
  const [bottomEdgeSize, setBottomEdgeSize] = useState(0);
  const [bottomRightEdgeSize,setRightBottomEdgeSize] = useState(0);
  const [topRightEdgeSize,setRightTopEdgeSize] = useState(0);

  const delayedAddNode = (coordinate: Coordinate) => {
    setTimeout(() => { addNode(coordinate) }, animationSpeed);
  }

  useEffect(() => {
    if (rightEdgeSize === 0) return;
    delayedAddNode({ x: X + edgeSize + size, y: Y })
  }, [rightEdgeSize]);

  useEffect(() => {
    if (leftEdgeSize === 0) return;
    delayedAddNode({ x: X - edgeSize - size, y: Y })
  }, [leftEdgeSize]);

  useEffect(() => {
    if (topEdgeSize === 0) return;
    delayedAddNode({ x: X, y: Y - size - edgeSize });
  }, [topEdgeSize]);

  useEffect(() => {
    if (bottomEdgeSize === 0) return;
    delayedAddNode({ x: X, y: Y + size + edgeSize });
  }, [bottomEdgeSize]);

  useEffect(() => {
    if (bottomRightEdgeSize === 0) return;
    delayedAddNode({ x: X + edgeSize * 1.2, y: Y + size + edgeSize * 0.55 });
  }, [bottomRightEdgeSize]);

  const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.preventDefault();
    setSize(size - size * 0.1);
  }
  return (
    <div>
      <div
        // onClick={() => { setSize(size + size * 0.10) }}
        // onContextMenu={handleClick}
        className="circle"
        style={{
          left: X,
          top: Y,
          width: size,
          height: size,
        }}>
        <div
          onClick={() => { if (topEdgeSize !== 0) return; setTopEdgeSize(edgeSize) }}
          data-role="top-connector"
          className="connector"
          style={{
            left: size / 2.4,
            top: "-6px",
          }} />
        <div
          onClick={() => { if (bottomEdgeSize !== 0) return; setBottomEdgeSize(edgeSize) }}
          data-role="bottom-connector"
          className="connector"
          style={{
            left: size / 2.4,
            top: size - size * 0.05,
          }} />
        <div
          onClick={() => { if (rightEdgeSize !== 0) return; setRightEdgeSize(edgeSize) }}
          data-role="right-connector"
          className="connector"
          style={{
            left: size * 0.95,
            top: size / 2.5,
          }} />
        <div
          onClick={() => { if (leftEdgeSize !== 0) return; setLeftEdgeSize(edgeSize) }}
          data-role="left-connector"
          className="connector"
          style={{
            left: "-6px",
            top: size / 2.5,
          }} />
        <div
          onClick={() => { if (bottomRightEdgeSize !== 0) return; setRightBottomEdgeSize(edgeSize) }}
          data-role="bottom-right-connector"
          className="connector"
          style={{
            left: size - (size * 0.18),
            top: size * 0.8,
          }} />
        <div
          onClick={() => { if (topRightEdgeSize !== 0) return; setRightTopEdgeSize(edgeSize) }}
          data-role="top-right-connector"
          className="connector"
          style={{
            left: size - (size * 0.18),
            top: size * 0.05,
          }} />
        <div
          onClick={() => { if (leftEdgeSize !== 0) return; setLeftEdgeSize(edgeSize) }}
          data-role="bottom-left-connector"
          className="connector"
          style={{
            left: size * 0.05,
            top: size * 0.8,
          }} />
        <div
          onClick={() => { if (leftEdgeSize !== 0) return; setLeftEdgeSize(edgeSize) }}
          data-role="top-left-connector"
          className="connector"
          style={{
            left: size * 0.05,
            top: size * 0.05,
          }} />
      </div>
      <div
        data-role="right-edge"
        className="edge black-outline"
        style={{
          left: X + size + 6,
          top: Y + 4 + size / 2,
          width: `${rightEdgeSize}`,
          ...transition,
        }} />
      <div
        data-role="top-edge"
        className="edge black-outline"
        style={{
          left: X + 4 + size / 2,
          top: topEdgeSize > 0 ? Y - edgeSize : Y + 2,
          height: `${topEdgeSize}`,
          ...transition,
        }} />
      <div
        data-role="bottom-edge"
        className="edge black-outline"
        style={{
          left: X + 4 + size / 2,
          top: Y + size + 6,
          height: `${bottomEdgeSize}`,
          ...transition,
        }} />
      <div
        data-role="left-edge"
        className="edge black-outline"
        style={{
          left: leftEdgeSize > 0 ? X - edgeSize : X + 2,
          top: Y + 4 + size / 2,
          width: `${leftEdgeSize}`,
          ...transition,
        }} />
        <div
          data-role="bottom-right-edge"
          className="edge black-outline"
          style={{
            left: X + size + size * 0.5,
            top: Y + size * 0.7,
            transform:"rotate(-45deg)",
            height: `${bottomRightEdgeSize}`,
            ...transition,
            visibility:bottomRightEdgeSize === 0 ? "hidden" : "visible"
          }} />
        <div
          data-role="top-right-edge"
          className="edge black-outline"
          style={{
            left: X + size + size * 0.5,
            top: Y + size * 0.7,
            transform:"rotate(-45deg)",
            height: `${topRightEdgeSize}`,
            ...transition,
            visibility:topRightEdgeSize === 0 ? "hidden" : "visible"
          }} />

    </div>
  )
}
export default Node;