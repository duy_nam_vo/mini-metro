import React, { useState } from 'react';
import Node from '../node';
import { produce } from 'immer';
import { v4 as uuidV4 } from 'uuid';
import { Graph, Coordinate } from '../../utils/types';


const Board = () => {
  const [nodes, setNodes] = useState<Graph[]>([
    {
      coordinate: { x: 100, y: 100 },
      id: uuidV4(),
      predecessor: "",
      succesor: "",
    }
  ]);
  const addNode =(id:string) => (coordinate: Coordinate) => {
    const n = produce(nodes, draft => {
      const newId = uuidV4();
      const currentNode = draft.find(node => node.id === id);
      if (currentNode) currentNode.succesor = newId;
      draft.push({ coordinate, id: newId, predecessor: id, succesor: "" });
    });
    setNodes(n);
  }
  return (
    <>
      {nodes.map((node, i) => <Node
        addNode={addNode(node.id)}
        key={node.id}
        X={node.coordinate.x}
        Y={node.coordinate.y}
        initialSize={60} />)}
    </>
  );
}

export default Board;