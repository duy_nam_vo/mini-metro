export interface Coordinate {
  x: number;
  y: number;
};

export interface Graph {
  coordinate: Coordinate;
  id: string;
  predecessor: string;
  succesor: string
}